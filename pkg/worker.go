package pkg

import (
	"gitlab.com/mauricio.galdieri/proletariat/tools/uuid"
)

type worker struct {
	id          string
	work        chan Processable
	workerQueue chan chan Processable
	quitChan    chan bool
}

// newWorker initializes a worker with an id.
func newWorker(id string, workerQueue chan chan Processable) *worker {
	w := worker{
		id:          id,
		work:        make(chan Processable),
		workerQueue: workerQueue,
		quitChan:    make(chan bool),
	}

	return &w
}

// start starts the worker in an indefinite select loop.
func (w *worker) start() {
	go func() {
		for {
			// Add the worker listening channel to the worker queue
			w.workerQueue <- w.work

			select {
			// When we receive a work request
			case work := <-w.work:
				// Generate a unique identifier for the work unit
				id := uuid.New().String()
				work.SetID(id)

				// Begin processing the work unit
				// TODO: handle errors returned from Process()
				work.Process()
			// When we receive a quit signal
			case <-w.quitChan:
				// Stopping worker
				w.shutdown()
				return
			}
		}
	}()
}

// stop shuts down a single worker.
func (w *worker) stop() {
	w.quitChan <- true
	close(w.quitChan)
}

func (w *worker) shutdown() {
	close(w.work)
}
