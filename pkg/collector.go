package pkg

// StartStopper is the interface for implementing a collector.
type StartStopper interface {
	// Start should start the collector. It shouldn't worry about blocking because it will run in its own goroutine
	// by the pool. It should accept a Processable channel into which new work units should be pushed.
	Start(chan Processable) error

	// Stop should stop the collector job.
	Stop() error
}
