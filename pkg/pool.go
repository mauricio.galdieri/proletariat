package pkg

import (
	"strconv"
)

// Config hold the pool's configuration parameters.
// - PoolSize is the size of the pool in number of worker units.
// - BufferRate is the rate between pool size and buffer.
//   E.g. with a pool size of 20 workers and a buffer rate of 10, the buffer will hold up to 200 work units (10 * 20).
// - Collectors is a slice of job collectors. It can be anything that implements the StartStopper interface.
// - BlockingBuffer is a flag that defines if the buffer should block, that is if the buffer reach its limit, additional
//   jobs will block at the collector's level until a worker is available from the pool. On the other hand, if it's
//   non blocking, additional jobs will wait at a new goroutine, allocating more memory as needed.
// - RunInBG is a flag that indicates if the pool should run in the background, or block at the caller.
type Config struct {
	PoolSize       int
	BufferRate     int
	Collectors     []StartStopper
	BlockingBuffer bool
	RunInBG        bool
}

type Pool struct {
	dispatcher *dispatcher
	collectors []StartStopper
	quitChan   chan bool
	runInBG    bool
}

// NewPoolWithConfig configures and initializes a worker pool, a dispatcher service and the job collectors.
// It returns a pointer to the pool and an error, if any.
func NewPoolWithConfig(config *Config) (*Pool, error) {
	pool := Pool{
		quitChan: make(chan bool),
		runInBG:  config.RunInBG,
	}

	// Initializes workers in worker pool
	workQueue, workerQueue, _ := initWorkers(config.PoolSize, config.BufferRate)

	// Initializes worker pool dispatcher
	dispatcher := newDispatcher(workQueue, workerQueue, config.BlockingBuffer)

	// Initializes job collectors
	collectors := make([]StartStopper, 0)
	for _, collector := range config.Collectors {
		collectors = append(collectors, collector)
	}

	pool.collectors = collectors
	pool.dispatcher = dispatcher

	return &pool, nil
}

// Start starts the dispatcher and collector services. The collectors run in their own goroutine.
// If it was configured to work in the background, this method does not block.
func (p *Pool) Start() {
	// start dispatcher
	p.dispatcher.start()
	// start collectors
	for _, collector := range p.collectors {
		// TODO: handle errors returned from collector.start()
		go collector.Start(p.dispatcher.workQueue)
	}

	if p.runInBG {
		go func() {
			for {
				select {
				case <-p.quitChan:
					p.shutdown()
					return
				}
			}
		}()
	} else {
		<-p.quitChan
		p.shutdown()
	}
}

// Stop gracefully shuts down the pool of workers.
// No more jobs will be accepted, but the ones already running will still finish.
func (p *Pool) Stop() {
	// Shuts down worker pool
	go func() {
		p.quitChan <- true
		close(p.quitChan)
	}()
}

func (p *Pool) shutdown() {
	// stop collectors
	for _, collector := range p.collectors {
		// TODO: handle errors returned from collector.stop()
		collector.Stop()
	}
	// stop dispatcher
	p.dispatcher.stop()
}

func initWorkers(poolSize int, bufferRate int) (chan Processable, chan chan Processable, []*worker) {
	// Initialize the work queue, with a buffer greater than the pool size
	workQueue := newWorkQueue(poolSize * bufferRate)
	// Initialize the worker queue, where we'll put the work request channels
	workerQueue := newWorkerQueue(poolSize)

	// Prepare a workers reference we can use to shut them down if anything goes wrong later
	workersRef := make([]*worker, poolSize)

	// start the workers
	for i := 0; i < poolSize; i++ {
		// Starting worker
		worker := newWorker(strconv.Itoa(i+1), workerQueue)
		worker.start()
		workersRef[i] = worker
	}

	return workQueue, workerQueue, workersRef
}
