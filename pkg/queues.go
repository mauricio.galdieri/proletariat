package pkg

// newWorkQueue returns a buffered channel of work requests.
func newWorkQueue(bufferSize int) chan Processable {
	return make(chan Processable, bufferSize)
}

// newWorkerQueue returns a buffered channel of channels
func newWorkerQueue(bufferSize int) chan chan Processable {
	return make(chan chan Processable, bufferSize)
}
