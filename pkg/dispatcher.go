package pkg

type dispatcher struct {
	workQueue   chan Processable
	workerQueue chan chan Processable
	workersRef  []*worker
	quitChan    chan bool
	blocking    bool
}

func newDispatcher(workQueue chan Processable, workerQueue chan chan Processable, blocking bool) *dispatcher {
	d := dispatcher{
		workQueue:   workQueue,
		workerQueue: workerQueue,
		workersRef:  make([]*worker, 0),
		quitChan:    make(chan bool),
		blocking:    blocking,
	}

	return &d
}

// start starts the workers and begins listening for work requests, dispatching them as they arrive.
func (d *dispatcher) start() {
	// Start the workers in the pool
	for _, w := range d.workersRef {
		w.start()
	}

	// Start dispatching
	go func() {
		for {
			select {
			case work := <-d.workQueue:
				// We received a work request
				if d.blocking {
					// Get an available worker from the queue and dispatch a job for it
					worker := <-d.workerQueue
					worker <- work
				} else {
					go func() {
						// Get an available worker from the queue and dispatch a job for it
						worker := <-d.workerQueue
						worker <- work
					}()
				}
			case <-d.quitChan:
				d.shutdown()
				return
			}
		}
	}()
}

// stop stops the dispatcher and associated services.
func (d *dispatcher) stop() {
	// Shuts down dispatcher
	d.quitChan <- true
	close(d.quitChan)
}

func (d *dispatcher) shutdown() {
	stopWorkers(d.workersRef)
	close(d.workerQueue)
}

func stopWorkers(workers []*worker) {
	for _, w := range workers {
		w.stop()
	}
}
