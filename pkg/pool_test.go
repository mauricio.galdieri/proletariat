package pkg

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

type TestWork struct {
	ID         string
	Sample     int
	ResultChan chan int
}

func (tw *TestWork) SetID(id string) {
	tw.ID = id
}

func (tw *TestWork) Process() error {
	tw.ResultChan <- tw.Sample
	return nil
}

type TestCollector struct {
	testSamples []int
}

var (
	testChan   chan int
	testResult []int
)

func (tc *TestCollector) Collect(workChan chan Processable) error {
	for _, s := range tc.testSamples {
		work := TestWork{
			Sample:     s,
			ResultChan: testChan,
		}
		workChan <- &work
	}
	return nil
}

func TestPool_SingleWorker(t *testing.T) {
	maxSamples := 100

	testSamples := make([]int, 0)
	for i := 0; i < maxSamples; i++ {
		testSamples = append(testSamples, i)
	}

	testChan = make(chan int)
	testResult = make([]int, 0)

	cronJobs := []*CronJob{
		{
			CronLine:  "*/1 * * * * * *",
			Collector: &TestCollector{testSamples},
		},
	}
	config := Config{
		PoolSize:   1,
		BufferRate: 1,
		CronJobs:   cronJobs,
		RunInBG:    true,
	}

	pool, _ := NewPoolWithConfig(&config)
	pool.Start()
	for len(testResult) < maxSamples*1 {
		s := <-testChan
		testResult = append(testResult, s)
	}
	pool.Stop()

	for i := 0; i < maxSamples; i++ {
		assert.Contains(t, testResult, i)
	}
}

func TestPool_MultipleWorkers(t *testing.T) {
	maxSamples := 100

	testSamples := make([]int, 0)
	for i := 0; i < maxSamples; i++ {
		testSamples = append(testSamples, i)
	}

	testChan = make(chan int)
	testResult = make([]int, 0)

	cronJobs := []*CronJob{
		{
			CronLine:  "*/1 * * * * * *",
			Collector: &TestCollector{testSamples},
		},
	}
	config := Config{
		PoolSize:   1,
		BufferRate: 1,
		CronJobs:   cronJobs,
		RunInBG:    true,
	}

	pool, _ := NewPoolWithConfig(&config)
	pool.Start()
	for len(testResult) < maxSamples*1 {
		s := <-testChan
		testResult = append(testResult, s)
	}
	pool.Stop()

	for i := 0; i < maxSamples; i++ {
		assert.Contains(t, testResult, i)
	}
}

func TestPool_HighBuffer(t *testing.T) {
	maxSamples := 100

	testSamples := make([]int, 0)
	for i := 0; i < maxSamples; i++ {
		testSamples = append(testSamples, i)
	}

	testChan = make(chan int)
	testResult = make([]int, 0)

	cronJobs := []*CronJob{
		{
			CronLine:  "*/1 * * * * * *",
			Collector: &TestCollector{testSamples},
		},
	}
	config := Config{
		PoolSize:   1,
		BufferRate: 1,
		CronJobs:   cronJobs,
		RunInBG:    true,
	}

	pool, _ := NewPoolWithConfig(&config)
	pool.Start()
	for len(testResult) < maxSamples*1 {
		s := <-testChan
		testResult = append(testResult, s)
	}
	pool.Stop()

	for i := 0; i < maxSamples; i++ {
		assert.Contains(t, testResult, i)
	}
}
