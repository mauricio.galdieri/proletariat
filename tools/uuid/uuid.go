package uuid

import (
	"crypto/rand"
	"fmt"
	"io"
)

type UUID [16]byte

// New genereates a random version 4 UUID.
func New() UUID {
	var uuid UUID
	b, err := io.ReadFull(rand.Reader, uuid[:])
	if b != len(uuid) || err != nil {
		panic(err)
	}

	uuid[8] = uuid[8]&^0xc0 | 0x80
	uuid[6] = uuid[6]&^0xf0 | 0x40

	return uuid
}

// String returns a version 4 formatted UUID
func (uuid UUID) String() string {
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:])
}
