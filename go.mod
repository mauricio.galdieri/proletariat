module gitlab.com/mauricio.galdieri/proletariat

go 1.12

require (
	github.com/aws/aws-sdk-go v1.19.45
	github.com/gorhill/cronexpr v0.0.0-20180427100037-88b0669f7d75
	github.com/stretchr/testify v1.3.0
	golang.org/x/net v0.0.0-20190607172144-d5cec3884524 // indirect
)
