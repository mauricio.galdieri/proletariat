package cron

import (
	"errors"
	worker "gitlab.com/mauricio.galdieri/proletariat/pkg"
)

// Runner defines the interface for collectors to push jobs to workers in the worker pool.
type Runner interface {
	// Run should start collecting jobs and sending them to workers in the pool.
	// It will be called at the intervals defined in the cron line passed to the constructor method.
	Run(chan worker.Processable) error
}

// Config holds configuration parameters for the cron collector
// - CronLine is a string describing que time the collector should start collecting jobs.
// - Collector is the custom struct responsible for doing the actual collecting of jobs.
//   Its Run() method gets called at every cron tick.
type Config struct {
	CronLine  string
	Collector Runner
}

// Collector represents the ticker for collecting work loads at specific times.
// It implements the worker pool's StartStopper interface.
type Collector struct {
	collector Runner
	ticker    *ticker
	quitChan  chan bool
}

// NewCollector initializes a new ticker for collecting work loads.
func NewCollector(config *Config) (*Collector, error) {
	if config.Collector == nil {
		return nil, errors.New("invalid collector")
	}

	tickerCollector := Collector{
		collector: config.Collector,
		ticker:    newTicker(config.CronLine),
		quitChan:  make(chan bool),
	}

	return &tickerCollector, nil
}

// Start starts the ticker for collecting jobs and pushing them to workers.
func (c *Collector) Start(workQueue chan worker.Processable) error {
	c.ticker.start()

	go func() {
		for {
			select {
			case <-c.ticker.Run:
				// TODO: handle errors returned from the collector
				c.collector.Run(workQueue)
			case <-c.quitChan:
				c.shutdown()
				return
			}
		}
	}()

	return nil
}

// Stop shuts down the collector job ticker.
func (c *Collector) Stop() error {
	c.quitChan <- true
	close(c.quitChan)
	return nil
}

func (c *Collector) shutdown() {
	c.ticker.stop()
}
