package cron

import (
	"github.com/gorhill/cronexpr"
	"time"
)

type ticker struct {
	timer      *time.Timer
	cronParser *cronexpr.Expression
	isTicking  bool
	quitChan   chan bool
	Run        chan bool
}

// newTicker returns a ticker with a `Run` channel that receives a signal at every tick, as configured in the cron line.
func newTicker(cronLine string) *ticker {
	expr := cronexpr.MustParse(cronLine)
	ticker := ticker{
		cronParser: expr,
		isTicking:  false,
		quitChan:   make(chan bool),
		Run:        make(chan bool),
	}
	return &ticker
}

// start starts the ticker. It it safe to call this several times.
func (t *ticker) start() {
	if !t.isTicking {
		t.isTicking = true
		t.updateTicker()
		go func() {
			for {
				select {
				case <-t.timer.C:
					t.Run <- true
					t.updateTicker()
				case <-t.quitChan:
					t.shutdown()
					return
				}
			}
		}()
	}
}

// stop stops a running ticker.
func (t *ticker) stop() {
	if t.isTicking {
		t.quitChan <- true
		close(t.quitChan)
	}
}

func (t *ticker) updateTicker() {
	now := time.Now()
	nextTick := t.cronParser.Next(now)
	diff := nextTick.Sub(now)

	if t.timer == nil {
		t.timer = time.NewTimer(diff)
	} else {
		t.timer.Reset(diff)
	}
}

func (t *ticker) shutdown() {
	t.isTicking = false
	close(t.Run)
}
