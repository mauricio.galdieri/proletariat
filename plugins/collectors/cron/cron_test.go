package cron

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestCronTicker(t *testing.T) {
	cronLine := "*/5 * * * * * *"

	cronTicker := newTicker(cronLine)

	cronTicker.start()
	defer cronTicker.stop()

	<-cronTicker.Run
	now := time.Now()
	is5Multiplier := 0 == now.Second()%5
	assert.True(t, is5Multiplier)
}
