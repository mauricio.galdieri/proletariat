package sqs

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type message struct {
	id   string
	body []byte
}

type consumer struct {
	client      *sqs.SQS
	inputCfg    *sqs.ReceiveMessageInput
	qURL        string
	pollTime    int
	maxConns    int
	maxMsgs     int
	successChan chan string
	quitChan    chan bool
	consumed    chan *message
}

func newConsumer(sess *session.Session, qURL string, pollTime int, maxConns int, maxMsgs int, successChan chan string) *consumer {
	if maxConns < 1 {
		maxConns = 1
	}

	if maxMsgs < 1 {
		maxMsgs = 1
	}
	if maxMsgs > 10 {
		maxMsgs = 10
	}

	input := sqs.ReceiveMessageInput{
		QueueUrl:            aws.String(qURL),
		MaxNumberOfMessages: aws.Int64(int64(maxMsgs)),
		WaitTimeSeconds:     aws.Int64(int64(pollTime)),
	}

	c := consumer{
		client:      sqs.New(sess),
		inputCfg:    &input,
		maxConns:    maxConns,
		successChan: successChan,
		quitChan:    make(chan bool),
		consumed:    make(chan *message),
	}

	return &c
}

func (c *consumer) start() {
	go func() {
		for {
			select {
			case <-c.quitChan:
				c.shutdown()
			}
		}
	}()
	for i := 0; i < c.maxConns; i++ {
		go c.consume()
	}
}

func (c *consumer) stop() {

}

func (c *consumer) shutdown() {
	close(c.quitChan)
	close(c.consumed)
}

func (c *consumer) consume() error {
	for {
		output, err := c.client.ReceiveMessage(c.inputCfg)
		if err != nil {
			return err
		}

		for _, msg := range output.Messages {
			m := message{
				id:   *msg.MessageId,
				body: []byte(*msg.Body),
			}
			c.consumed <- &m
		}
	}
}
