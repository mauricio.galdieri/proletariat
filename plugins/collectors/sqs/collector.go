package sqs

import (
	"github.com/aws/aws-sdk-go/aws/session"
	worker "gitlab.com/mauricio.galdieri/proletariat/pkg"
)

// Runner defines the interface for collectors to push jobs to workers in the worker pool.
type Runner interface {
	// Run should start collecting jobs and sending them to workers in the pool.
	// It will be called every time a new message is retrieved from the queue.
	Run(chan worker.Processable) error
}

// Config holds configuration parameters for the cron collector
// - AWSSession is the aws session to be used for connecting to the SQS queue.
// - QueueURL is the complete url for the queue. E.g. https://sqs.us-east-1.amazonaws.com/123456789012/QueueName
// - PollingWaitTime is the wait time in seconds for long polling. Use 0 for fast polling queues.
// - MaxConnections is the number of concurrent connections to the queue. Use a number > 1 for fast message retrieval.
// - MaxMessages is the maximum number of messages retrieved at each poll cycle. Must be between 1 and 10. Defaults to 10.
type Config struct {
	Collector       Runner
	AWSSession      *session.Session
	QueueURL        string
	PollingWaitTime int
	MaxConnections  int
	MaxMessages     int
}

// Collector represents the ticker for collecting work loads at specific times.
// It implements the worker pool's StartStopper interface.
type Collector struct {
	collector   Runner
	consumer    *consumer
	successChan chan string
	quitChan    chan bool
}

func NewCollector(config *Config) (*Collector, error) {
	successChan := make(chan string)

	consumer := newConsumer(
		config.AWSSession,
		config.QueueURL,
		config.PollingWaitTime,
		config.MaxConnections,
		config.MaxMessages,
		successChan)

	c := Collector{
		collector:   config.Collector,
		consumer:    consumer,
		successChan: successChan,
		quitChan:    make(chan bool),
	}

	return &c, nil
}

// Start starts consuming messages from the queue.
func (c *Collector) Start(workQueue chan worker.Processable) error {
	c.consumer.start()

	go func() {
		for {
			select {
			case msgID := <-c.consumer.consumed:
				if err := c.collector.Run(workQueue); err == nil {
					c.successChan <- msgID
				}
			case <-c.quitChan:
				c.shutdown()
				return
			}
		}
	}()

	return nil
}

// Stop shuts down the SQS consumer.
func (c *Collector) Stop() error {
	c.quitChan <- true
	close(c.quitChan)
	return nil
}

func (c *Collector) shutdown() {

}
